var isTheme = true,
	themeName = 'Styles',
	browserSyncMethod = 'proxy',// 'static' or 'proxy'
	localServerName = 'http://styles:8888'// or relevent host on your local (eg MAMP Pro Hostname), if using proxy method
;

var gulp = require('gulp'),
		imagemin = require('gulp-imagemin'),
		htmlmin = require('gulp-htmlmin'),
		pngquant = require('imagemin-pngquant'),
		combineMq = require('gulp-combine-mq'),
		util = require('gulp-util'),
		notify = require('gulp-notify'),
		plumber = require('gulp-plumber'),
		jshint = require('gulp-jshint'),
		sourcemaps = require('gulp-sourcemaps'),
		sass = require('gulp-sass'),
		autoprefixer = require('gulp-autoprefixer'),
		stripCssComments = require('gulp-strip-css-comments'),
		minifyCss = require('gulp-minify-css'),
		concat = require('gulp-concat'),
		stripDebug = require('gulp-strip-debug'),
		uglify = require('gulp-uglify'),
		newer = require('gulp-newer'),
		filter = require('gulp-filter'),
		del = require('del'),
		watch = require('gulp-watch'),
		runSequence = require('run-sequence'),
		rev = require('gulp-rev-mtime'),
		htmlPartial = require('gulp-html-partial'),
        svgSprite = require('gulp-svg-sprite'),
		version = require('gulp-version-number'),
		versionConfig = {
			'value': '%MDS%',
			'append': {
				'key': 'v',
				'to': ['css', 'js']
			}
		},
		browserSync = require('browser-sync').create();

var srcRoot = './src/',
		distRoot = './dist/wp-content/themes/Styles/',

		srcThemeDir = srcRoot,
		distThemeDir = distRoot,

		themeMainJs = srcThemeDir+'js/main.js',
		themeJsFiles = [
			srcThemeDir+"js/libs/*.js",
			themeMainJs
		],

		_vendorJs = 'js/vendor/**/*.js',

		srcThemeVendorJs = srcThemeDir+_vendorJs,
		distThemeVendorJs = distThemeDir+_vendorJs,

		themeMainSassFiles = srcThemeDir+'scss/*.scss',
		themeSassFiles = srcThemeDir+'scss/**/*.scss',

		_imageFiles = 'images/**/*',

		srcThemeImageFiles = srcThemeDir+_imageFiles,
		distThemeImageFiles = distThemeDir+_imageFiles,

        _svgFiles = 'images/icons/*.svg',

        srcThemeSvgFiles = srcThemeDir+_svgFiles,
        distThemeSvgFiles = distThemeDir+_svgFiles,

		_tplFiles = '**/*.php',

		srcThemeTemplateFiles = srcThemeDir+_tplFiles,
		distThemeTemplateFiles = distThemeDir+_tplFiles,

		_htmlFiles = '**/*.html',

		srcHtmlFiles = srcThemeDir+_htmlFiles,
		distHtmlFiles = distThemeDir+_htmlFiles,

		_fontFiles = 'fonts/**/*',

		srcThemeFontFiles = srcThemeDir+_fontFiles,
		distThemeFontFiles = distThemeDir+_fontFiles,

		_audioFiles = 'audio/**/*',

		srcThemeAudioFiles = srcThemeDir+_audioFiles,
		distThemeAudioFiles = distThemeDir+_audioFiles,

		srcThemeFiles = [srcThemeTemplateFiles, srcThemeDir+'style.css', srcThemeDir+'screenshot.png'],
		distThemeFiles = [distThemeTemplateFiles, distThemeDir+'style.css', distThemeDir+'screenshot.png'],

		plumberErrorHandler = {
			errorHandler: notify.onError({
				title: 'Gulp',
				message: 'Plugin: <%= error.plugin %> \nError: <%= error.message %>'
			})
		};


gulp.task('clean-theme-files', function() {
	return del([distThemeTemplateFiles, '!dist']);
});

gulp.task('clean-theme-css', function() {
	return del([distThemeDir+'css']);
});

gulp.task('clean-theme-scripts', function() {
	return del([distThemeDir+'js/**/*']);
});

gulp.task('clean-theme-vendor-files', function() {
	return del([distThemeDir+'vendor/**/*']);
});

gulp.task('clean', function(){
	runSequence('clean-theme-css', 'clean-theme-scripts', 'clean-theme-vendor-files', 'clean-theme-files');
});


gulp.task('theme-files', function () {

	if (isTheme) {
		gulp.src(srcThemeDir+'js/tinymce-plugins/*.js')
			.pipe(plumber(plumberErrorHandler))
			.pipe(gulp.dest(distThemeDir+'js/tinymce-plugins/'));
	}
	//else {
		gulp.src(srcHtmlFiles)
			.pipe(htmlPartial({
				basePath: 'src/html-partials/'
			}))
			.pipe(plumber(plumberErrorHandler))
			.pipe(version(versionConfig))
			.pipe(gulp.dest(distThemeDir))
			.pipe(browserSync.stream());
	//}

	return gulp.src(srcThemeFiles)
		.pipe(plumber(plumberErrorHandler))
		.pipe(gulp.dest(distThemeDir))
		.pipe(browserSync.stream());

});


gulp.task('theme-images', function() {

	return gulp.src(srcThemeImageFiles)
			.pipe(plumber(plumberErrorHandler))
			.pipe(newer(distThemeImageFiles))
			.pipe(imagemin({
				progressive: true,
				svgoPlugins: [{removeViewBox: false}],
				use: [pngquant()]
			}))
			.pipe(gulp.dest(distThemeDir+'images'))
			.pipe(browserSync.stream());

});

gulp.task('theme-fonts', function() {

	return gulp.src(srcThemeFontFiles)
			.pipe(plumber(plumberErrorHandler))
			.pipe(gulp.dest(distThemeDir+'fonts'))
			.pipe(browserSync.stream());

});

gulp.task('theme-audio', function() {

	return gulp.src(srcThemeAudioFiles)
			.pipe(plumber(plumberErrorHandler))
			.pipe(gulp.dest(distThemeDir+'audio'))
			.pipe(browserSync.stream());

});

gulp.task('theme-sass-dev', function() {

	return gulp.src(themeMainSassFiles)
			.pipe(plumber(plumberErrorHandler))
			.pipe(sourcemaps.init())
			.pipe(sass())
			.pipe(autoprefixer({
				cascade: false
			}))
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest(distThemeDir+'css'))
			.pipe(browserSync.stream());

});


gulp.task('theme-sass', function() {

	return gulp.src(themeMainSassFiles)
			.pipe(plumber(plumberErrorHandler))
			.pipe(sass())
			.pipe(autoprefixer())
			.pipe(autoprefixer({
				cascade: false
			}))
			.pipe(gulp.dest(distThemeDir+'css'))

			.pipe(plumber.stop())
			.pipe(filter(distThemeDir+'css/*.css'))// Filtering stream to only css files
			.pipe(combineMq()) // Combines Media Queries
			.pipe(stripCssComments())
			.pipe(minifyCss())
			.pipe(browserSync.stream());

});


gulp.task('theme-lint', function() {
	gulp.src(themeMainJs)
			.pipe(plumber(plumberErrorHandler))
			.pipe(jshint())
			.pipe(jshint.reporter('jshint-stylish'));
});


gulp.task('theme-scripts-dev', function() {
	return gulp.src(themeJsFiles, { base: '.' })
			.pipe(plumber(plumberErrorHandler))
			.pipe(sourcemaps.init())
			.pipe(concat('main.js'))
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest(distThemeDir+'js'))
			.pipe(browserSync.stream());
});


gulp.task('theme-scripts', function() {
	return gulp.src(themeJsFiles, { base: '.' })
			.pipe(plumber(plumberErrorHandler))
			.pipe(concat('main.js'))
			.pipe(stripDebug())
			.pipe(uglify())
			.pipe(gulp.dest(distThemeDir+'js'))
			.pipe(browserSync.stream());
});


gulp.task('theme-vendor-scripts', function() {
	return gulp.src(srcThemeVendorJs)
			.pipe(plumber(plumberErrorHandler))
			.pipe(newer(distThemeVendorJs))
			.pipe(gulp.dest(distThemeDir+'js/vendor'))
			.pipe(browserSync.stream());
});

gulp.task('serve', ['clean'], function() {

	runSequence([
		'theme-sass',
		'theme-lint'
	],[
		'theme-scripts',
		'theme-vendor-scripts',
		'theme-images'
	], 'theme-files', ['theme-fonts', 'theme-audio']);

	if (browserSyncMethod === 'proxy') {
		// proxy (local)
		browserSync.init({
			proxy: localServerName
		});
	}
	else {
		// static server
		browserSync.init({
			server: "./dist"
		});
	}


	gulp.src(srcHtmlFiles)
		.pipe(watch(srcHtmlFiles, function() {
			runSequence('theme-files');
		}));

	gulp.src(srcThemeTemplateFiles)
			.pipe(watch(srcThemeTemplateFiles, function() {
				runSequence('theme-files');
			}));

	gulp.src(srcThemeVendorJs)
			.pipe(watch(srcThemeVendorJs, function() {
				gulp.start('theme-vendor-scripts');
			}));

	gulp.src(srcThemeFontFiles)
			.pipe(watch(srcThemeFontFiles, function() {
				gulp.start('theme-fonts');
			}));

	gulp.src(srcThemeAudioFiles)
			.pipe(watch(srcThemeAudioFiles, function() {
				gulp.start('theme-audio');
			}));

	gulp.src(themeJsFiles)
			.pipe(watch(themeJsFiles, function() {
				runSequence('theme-lint', 'theme-scripts');
			}));

	gulp.src(themeSassFiles)
			.pipe(watch(themeSassFiles, function() {
				gulp.start('theme-sass');
			}));

	gulp.src(srcThemeImageFiles)
			.pipe(watch(srcThemeImageFiles, function() {
				gulp.start('theme-images');
			}));

});


gulp.task('serve-dev', ['clean'], function() {

	runSequence([
		'theme-sass-dev',
		'theme-lint'
	], [
		'theme-scripts-dev',
		'theme-vendor-scripts',
		'theme-images'
	], 'theme-files', ['theme-fonts', 'theme-audio']);


	if (browserSyncMethod === 'proxy') {
		// proxy (local)
		browserSync.init({
			proxy: localServerName
		});
	}
	else {
		// static server
		browserSync.init({
			server: "./dist"
		});
	}


	gulp.src(srcHtmlFiles)
			.pipe(watch(srcHtmlFiles, function() {
				runSequence('theme-files');
			}));

	gulp.src(srcThemeTemplateFiles)
			.pipe(watch(srcThemeTemplateFiles, function() {
				runSequence('theme-files');
			}));

	gulp.src(srcThemeFontFiles)
			.pipe(watch(srcThemeFontFiles, function() {
				runSequence('theme-fonts');
			}));

	gulp.src(srcThemeAudioFiles)
			.pipe(watch(srcThemeAudioFiles, function() {
				runSequence('theme-audio');
			}));

	gulp.src(srcThemeVendorJs)
			.pipe(watch(srcThemeVendorJs, function() {
				gulp.start('theme-vendor-scripts');
			}));

	gulp.src(themeJsFiles)
			.pipe(watch(themeJsFiles, function() {
				runSequence('theme-lint', 'theme-scripts-dev');
			}));

	gulp.src(themeSassFiles)
			.pipe(watch(themeSassFiles, function() {
				gulp.start('theme-sass-dev');
			}));

	gulp.src(srcThemeImageFiles)
			.pipe(watch(srcThemeImageFiles, function() {
				gulp.start('theme-images');
			}));

});


gulp.task('watch', function() {

	runSequence('clean', [
		'theme-sass',
		'theme-lint'
	], [
		'theme-scripts',
		'theme-vendor-scripts',
		'theme-images'
	], 'theme-files', ['theme-fonts', 'theme-audio']);

	gulp.watch(srcHtmlFiles, ['theme-files']);

	gulp.watch(srcThemeTemplateFiles, ['theme-files']);

	gulp.watch(srcThemeFontFiles, ['theme-fonts']);

	gulp.watch(srcThemeVendorJs, ['theme-vendor-scripts']);

	gulp.watch(themeJsFiles, ['theme-lint', 'theme-scripts']);

	gulp.watch(themeSassFiles, ['theme-sass']);

	gulp.watch(srcThemeImageFiles, ['theme-images']);

});

gulp.task('watch-dev', function() {

	runSequence('clean', [
		'theme-sass-dev',
		'theme-lint'
	], [
		'theme-scripts-dev',
		'theme-vendor-scripts',
		'theme-images'
	], 'theme-files');

	gulp.watch(srcHtmlFiles, ['theme-files']);

	gulp.watch(srcThemeTemplateFiles, ['theme-files']);

	gulp.watch(srcThemeFontFiles, ['theme-fonts']);


	gulp.watch(srcThemeVendorJs, ['theme-vendor-scripts']);

	gulp.watch(themeJsFiles, ['theme-lint', 'theme-scripts-dev']);

	gulp.watch(themeSassFiles, ['theme-sass-dev']);

	gulp.watch(srcThemeImageFiles, ['theme-images']);

});

// Default Task
gulp.task('default', function(){
	runSequence('clean', [
		'theme-sass',
		'theme-lint'
	], [
		'theme-scripts',
		'theme-vendor-scripts',
		'theme-images'
	], 'theme-files', ['theme-fonts']);
});

// Dev Task
gulp.task('dev', function(){
	runSequence('clean', [
		'theme-sass-dev',
		'theme-lint'
	], [
		'theme-scripts-dev',
		'theme-vendor-scripts',
		'theme-images'
	], 'theme-files', ['theme-fonts', 'theme-audio']);
});
