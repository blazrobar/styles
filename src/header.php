<!DOCTYPE html>
<head>
	<title><?php wp_title(''); ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/themes/nano.min.css"/>
    <?php wp_head(); ?>
    <style>
        <?php the_field('css'); ?>
    </style>
</head>
<body <?php body_class(); ?>>
<header>
	<a href="<?php echo get_site_url(); ?>">
        <img src="<?php bloginfo('template_directory'); ?>/images/logo.svg" class="logo"/>
    </a>
	<div class="icons">
<!--		<p>Download the <a href="#">Gravity Forms custom css</a> plugin</p>-->
		<a href="#">
			<i class="material-icons">cloud_upload</i>
		</a>
		<a href="#">
			<i class="material-icons">add_alert<span class="notification"></span></i>
		</a>
		<a href="#">
			<i class="material-icons">sort</i>
		</a>
        <a href="#">
<!--            <input type="text" value="Search Styles"/>-->
            <i class="material-icons">search</i>
        </a>
        <a href="#" class="profile-pic">
            <img src="<?php bloginfo('template_directory'); ?>/images/blaz.jpg"/>
        </a>
	</div>
</header>


