<?php get_header(); ?>
    <div class="container-outer container-outer-grey">
        <div class="container">
            <section class="section section-updates">
                <header class="section-head">
                    <h1>Latest Form styles</h1>
                </header><!-- /.section-head -->

                <div class="section-body">
                    <?php
                    $posts_query = new WP_Query(array(
                            'post_type' => 'styles'
                        ));
                    ?>
                    <?php if ($posts_query->have_posts()): ?>
                        <?php while ($posts_query->have_posts()): $posts_query->the_post(); global $post; ?>
                            <div class="style-tile" style="background:<?php get_field('variable_colour');?><>
                                <?php the_title();?>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; wp_reset_postdata(); ?>
                </div><!-- /.section-body -->
            </section><!-- /.section section-updates -->
        </div><!-- /.container -->
    </div><!-- /.container-outer -->
<?php get_footer(); ?>