<?php
/* Template Name: Raw CSS
*/
?>
<?php
    $style_id = $_GET['style_post__id'];
    $title = get_the_title($style_id);
    $raw_css_output = get_field('css', $style_id);
?>

<?php get_header(); ?>
    <section class="style-summary-wrap">
        <div class="raw-css-wrapper">
            <pre class="language-scss">
                <code><?php echo $raw_css_output; ?></code>
            </pre>
        </div>
        <div class="css-info">
            <h2><?php echo $title;?> Styles</h2>
            <p>Sime text about how to copy the CSS and what you should do next with this CSS to go here.</p>
            <a href="#" class="btn-primary">
                <i class="material-icons">assignment_returned</i>Copy Raw CSS
            </a>
        </div>
    </section>
<?php get_footer(); ?>