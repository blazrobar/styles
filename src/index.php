<div class="container-outer container-outer-grey">
    <div class="container">
        <section class="section section-updates">
            <header class="section-head">
                <h1>Latest Blog</h1>
            </header><!-- /.section-head -->
            <?php if (have_posts()): ?>
                <?php while(have_posts()): the_post(); global $post; ?>
                    <a href="<?php the_permalink();?>">
                        <div class="section-body">
                            <h2><?php the_title();?></h2>
                        </div><!-- /.section-body -->
                    </a>
                <?php endwhile; ?>
            <?php endif; ?>
        </section><!-- /.section section-updates -->
    </div><!-- /.container -->
</div><!-- /.container-outer -->
<?php include(locate_template('/inc/onboarding.php')); ?>
<?php get_footer(); ?>