<?php get_header(); ?>
	<div class="container-outer container-outer-grey">
		<div class="container">

			<section class="section">
				<header class="section-head">
					<h1>404 Page not found</h1>
				</header><!-- /.section-head -->

				<div class="section-body">
					Go back to the home page
				</div><!-- /.section-body -->
			</section><!-- /.section -->
		</div><!-- /.container -->
	</div><!-- /.container-outer -->
<?php get_footer(); ?>