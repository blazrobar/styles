<?php get_header(); ?>
	<div class="container-outer container-outer-grey">
		<div class="container">
			<section class="section-breadcrumbs">
				<p class="breadcrumbs">
					<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb();} ?>
				</p><!-- /.breadcrumbs -->
			</section><!-- /.section-breadcrumbs -->

			<section class="section">
				<header class="section-head">
					<h1><?php the_title();?></h1>

					<div class="section-head-actions">
						<?php include(locate_template( '/inc/social.php' ));?>
					</div><!-- /.section-head-actions -->
				</header><!-- /.section-head -->
			</section><!-- /.section -->
		</div><!-- /.container -->
	</div><!-- /.container-outer -->
<?php include(locate_template('/rows.php')); ?>
<?php include(locate_template( '/inc/free_shipping.php' ));?>
<?php get_footer(); ?>