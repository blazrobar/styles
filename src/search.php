<?php get_header(); ?>
    <div class="wrap_white">
        <div class="breadcrum_wrap">
            <div class="wrap">
                <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb();} ?>
            </div>
        </div>
        <div class="wrap">
            <div class="grid">
                <div class="spacer"></div>
                <div class="grid__item six-tenths lap-one-whole palm-one-whole">
                    <h1>Search: <?php echo get_search_query(); ?></h1>
                    <div class="post_meta">
                        <?php the_views()?>
                    </div>
                </div>
                <div class="grid__item four-tenths lap-one-whole palm-one-whole">
                    <div class="post_meta">
                        <h4>Search by Popular Tags</h4>
	                    <ul>
		                    <?php
		                    global $wpdb;
		                    $term_ids = $wpdb->get_col("
                               SELECT DISTINCT term_taxonomy_id FROM $wpdb->term_relationships
                                  INNER JOIN $wpdb->posts ON $wpdb->posts.ID = object_id
                                  WHERE DATE_SUB(CURDATE(), INTERVAL 365 DAY) <= $wpdb->posts.post_date");

		                    if(count($term_ids) > 0){

			                    $tags = get_tags(array(
				                    'orderby' => 'count',
				                    'order'   => 'DESC',
				                    'number'  => 8,
				                    'include' => $term_ids,
			                    ));
			                    foreach ( (array) $tags as $tag ) {
				                    echo '<li><a href="' . get_tag_link ($tag->term_id) . '" rel="tag">' . $tag->name . ' (' .$tag->count. ')</a></li>';
			                    }
		                    }
		                    ?>
	                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>




 <!--Posts-->
        <div class="row">
            <div class="wrap">
                <div class="grid">
                    <div class="spacer"></div>
                <!--Loop-->
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="grid__item one-third lap-one-whole palm-one-whole">
                        <a href="<?php the_permalink(); ?>">
                            <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 1000,1000 ), false, '' );?>
                            <div class="card" style="background: url(<?php echo $src[0]; ?>); background-size: cover; height: 480px;">
                                <?php if ( in_category(9) ) : ?>
                                    <div class="video"></div>
                                <?php else : ?>
                                <?php endif; ?>
                                <div class="card_data">
                                    <h2><?php the_field('h1');?></h2>
                                    <p><?php the_excerpt();?></p>
                                    <div class="meta">
                                        <span class="date"><?php the_time('F j, Y'); ?></span>
                                    </div>
                                </div>
                                <div class="hippy-purple"></div>
                            </div>
                        </a>
                    </div>
                <?php endwhile; else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
                </div>
            </div>
            <div class="spacer"></div>
        </div>




<div class="row">
        <div class="wrap">
            <?php wp_pagenavi(); ?>
            <div class="spacer"></div>
        </div>
    </div>
<?php get_footer(); ?>