<?php
//////////////////////////////////////////////////////////////////
// ACF Options page
//////////////////////////////////////////////////////////////////

$opt_title = 'Site Options';
$add = array(
	'Site Options' => 'Site Options'
);

if (function_exists('acf_set_options_page_menu')) {
	acf_set_options_page_menu($opt_title);
}
if (function_exists('acf_set_options_page_title')) {
	acf_set_options_page_title($opt_title);
}

if (function_exists('acf_add_options_page')) {
	foreach ($add as $men => $ttl) {
		acf_add_options_page(array(
			'title' => $ttl,
			'menu'  => $men
		));
	}
}
