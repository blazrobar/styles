<?php
//////////////////////////////////////////////////////////////////
// Hide the admin bar
//////////////////////////////////////////////////////////////////
add_filter('show_admin_bar', '__return_false');




//////////////////////////////////////////////////////////////////
// Change 'from' name and email
//////////////////////////////////////////////////////////////////

/*
add_filter('wp_mail_from', 'new_mail_from');
function new_mail_from($old) {
    return 'email@address.com';
}
*/

add_filter('wp_mail_from_name', 'new_mail_from_name');
function new_mail_from_name($old) {
	return get_bloginfo('name');
}

//////////////////////////////////////////////////////////////////
// Remove sitename from email subject
//////////////////////////////////////////////////////////////////
add_filter('wp_mail', 'email_subject_remove_sitename');
function email_subject_remove_sitename($email) {

	$email['subject'] = str_replace("[WordPress] - ", "", $email['subject']);
	$email['subject'] = str_replace("[WordPress]", "", $email['subject']);

	$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
	$email['subject'] = str_replace("[".$blogname."] - ", "", $email['subject']);
	$email['subject'] = str_replace("[".$blogname."]", "", $email['subject']);

	return $email;
}

//////////////////////////////////////////////////////////////////
// Change page title in admin area
//////////////////////////////////////////////////////////////////
add_filter('admin_title', 'custom_admin_title', 10, 2);
function custom_admin_title($admin_title, $title) {
	return get_bloginfo('name').' Admin - '.$title;
}



/*
//////////////////////////////////////////////////////////////////
// Use custom styles on the login page
//////////////////////////////////////////////////////////////////
add_action('login_head', 'custom_login_css');
function custom_login_css() {
	echo '<link rel="stylesheet" href="' . get_bloginfo('template_url') . '/css/login.min.css?v=' . time() . '" />';
}
*/

//////////////////////////////////////////////////////////////////
// Change/Disable the footer text line
//////////////////////////////////////////////////////////////////
add_action( 'admin_init', 'remove_admin_footer' );
function remove_admin_footer() {
	if ( is_admin() && !check_user_role('administrator')) {
		add_filter( 'admin_footer_text', '__return_false', 11 );
		add_filter( 'update_footer', '__return_false', 11 );
	}
}


//////////////////////////////////////////////////////////////////
// Control the Dashboard widgets in admin
//////////////////////////////////////////////////////////////////
add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );
function remove_dashboard_widgets() {
	if (is_admin()) {
		global $wp_meta_boxes;
		unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] ); # Allows for basic post entry
		unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links'] ); # Shows you who is linking to you
		unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'] ); # Displays new, updated, and popular WordPress plugins on WordPress.org
		unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary'] ); # Highlights entries from the WordPress team on WordPress.org
		unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'] ); # Displays stats about your blog
		unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments'] ); # Displays the most recent comments on your blog
		unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts'] ); # Displays your most recent drafts
		unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'] ); # Displays the WordPress Planet feed, which includes blog entries from WordPress.org
	}
}

// Filter Yoast Meta Priority
add_filter( 'wpseo_metabox_prio', function() { return 'low';});

//////////////////////////////////////////////////////////////////
// Add custom styles to tiny mce editor
//////////////////////////////////////////////////////////////////
add_filter( 'mce_buttons_2', 'custom_mce_buttons_2' );
function custom_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}

add_filter( 'tiny_mce_before_init', 'mce_before_init_insert_formats' );
function mce_before_init_insert_formats( $settings ) {

	/*
		title [required]	label for this dropdown item
		selector | block | inline [required]
			- selector limits the style to a specific HTML tag, and will apply the style to an existing tag instead of creating one
			- block creates a new block-level element with the style applied, and will replace the existing block element around the cursor
			- inline creates a new inline element with the style applied, and will wrap whatever is selected in the editor, not replacing any tags
		classes [optional]	space-separated list of classes to apply to the element
		styles [optional]	array of inline styles to apply to the element (two-word attributes, like font-weight, are written in Javascript-friendly camel case: fontWeight)
		attributes [optional]	assigns attributes to the element (same syntax as styles)
		wrapper [optional, default = false]	if set to true, creates a new block-level element around any selected block-level elements
		exact [optional, default = false]	disables the “merge similar styles” feature, needed for some CSS inheritance issues
	*/
	$style_formats = array(
		array(
			'title' => 'Button',
			'selector' => 'a',
			'classes' => 'btn btn-alt btn-red',
			'wrapper' => false
		),
		array(
			'title' => 'List',
			'selector' => 'ul',
			'classes' => 'list-red-circles',
			'wrapper' => false
		),
		array(
			'title' => 'Red',
			'selector' => 'ul,p,a,li,h1,h2,h3,h4,h5,h6',
			'classes' => 'red',
			'wrapper' => false
		)
	);

	$settings['style_formats'] = json_encode( $style_formats );

	return $settings;

}