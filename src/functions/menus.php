<?php
//////////////////////////////////////////////////////////////////
// Add menus support / register menu(s)
//////////////////////////////////////////////////////////////////
function add_menus() {

    // add custom menu support
    if (function_exists('add_theme_support')) {
        add_theme_support('menus');
    }

}
add_action('init', 'add_menus');