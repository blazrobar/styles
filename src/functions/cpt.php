<?php
/* =============================================================================
   Custom Post Types
   ========================================================================== */
add_action('init', 'all_custom_post_types');
function all_custom_post_types() {

	$types = array(
		array(
			'the_type' => 'Styles',
			'single' => 'Styles',
			'archive' => 'Styles',
			'plural' => 'Styles',
			'supports' => array('title','thumbnail','page-attributes','tags','comments','editor'),
		)
	);

	foreach ($types as $type) {
		$the_type = $type['the_type'];
		$single = $type['single'];
		$plural = (isset($type['plural'])) ? $type['plural'] : $type['single'].'s';

		$public = (isset($type['public'])) ? $type['public'] : true;

		$has_archive= (isset($type['has_archive'])) ? $type['has_archive'] : true;
		$supports = (isset($type['supports'])) ? $type['supports'] : array('title', 'editor');
		$query_var = (isset($type['query_var'])) ? $type['query_var'] : true;
		$rewrite = (isset($type['rewrite'])) ? $type['rewrite'] : true;
		$capability_type = (isset($type['capability_type'])) ? $type['capability_type'] : 'post';
		$hierarchical = (isset($type['hierarchical'])) ? $type['hierarchical'] : false;
		$menu_position = (isset($type['menu_position'])) ? $type['menu_position'] : null;
		$can_export = (isset($type['can_export'])) ? $type['can_export'] : true;

		$publicly_queryable = (isset($type['publicly_queryable'])) ? $type['publicly_queryable'] : $public;
		$exclude_from_search = (isset($type['exclude_from_search'])) ? $type['exclude_from_search'] : !$public;
		$show_ui = (isset($type['show_ui'])) ? $type['show_ui'] : $public;
		$show_in_nav_menus = (isset($type['show_in_nav_menus'])) ? $type['show_in_nav_menus'] : $show_ui;
		$show_in_menu = (isset($type['show_in_menu'])) ? $type['show_in_menu'] : $show_ui;
		$show_in_admin_bar = (isset($type['show_in_admin_bar'])) ? $type['show_in_admin_bar'] : $show_in_menu;

		$labels = array(
			'name' => _x($plural, 'post type general name'),
			'singular_name' => _x($single, 'post type singular name'),
			'add_new' => _x('Add New', $single),
			'add_new_item' => __('Add New '. $single),
			'edit_item' => __('Edit '.$single),
			'new_item' => __('New '.$single),
			'view_item' => __('View '.$single),
			'search_items' => __('Search '.$plural),
			'not_found' =>  __('No '.$plural.' found'),
			'not_found_in_trash' => __('No '.$plural.' found in Trash'),
			'parent_item_colon' => ''
		);

		$args = array(
			'labels' => $labels,
			'public' => $public,
			'exclude_from_search' => $exclude_from_search,
			'show_in_menu' => $show_in_menu,
			'show_in_nav_menus' => $show_in_nav_menus,
			'show_in_admin_bar' => $show_in_admin_bar,
			'has_archive' => $has_archive,
			'publicly_queryable' => $publicly_queryable,
			'show_ui' => $show_ui,
			'can_export' => $can_export,
			'query_var' => $query_var,
			'rewrite' => $rewrite,
			'capability_type' => $capability_type,
			'hierarchical' => $hierarchical,
			'menu_position' => $menu_position,
			'supports' => $supports
		);
		if (isset($type['menu_icon'])) {
			$args['menu_icon'] = $type['menu_icon'];
		}
		register_post_type($the_type, $args);

		if (isset($type['taxonomies'])) {

			foreach($type['taxonomies'] as $taxonomy) {

				$tax_slug = $taxonomy['slug'];
				$tax_label = $taxonomy['label'];
				$tax_hierarchical = (isset($taxonomy['hierarchical'])) ? $taxonomy['hierarchical'] : true;
				$tax_queryvar = (isset($taxonomy['query_var'])) ? $taxonomy['query_var'] : true;

				register_taxonomy($tax_slug, $the_type,
					array(
						'hierarchical' => $tax_hierarchical,
						'label' => $tax_label,
						'query_var' => $tax_queryvar,
						'rewrite' => array(
							'slug' => $tax_slug, // This controls the base slug that will display before each term
							'with_front' => false // Don't display the category base before
						)
					)
				);

			}

		}

	}

}