<?php
//Turn off autocomplte for gravity forms
add_filter( 'gform_form_tag', 'gform_form_tag_autocomplete', 11, 2 );
function gform_form_tag_autocomplete( $form_tag, $form ) {
    if ( is_admin() ) return $form_tag;
    if ( GFFormsModel::is_html5_enabled() ) {
        $form_tag = str_replace( '>', ' autocomplete="fdfsd">', $form_tag );
    }
    return $form_tag;
}
add_filter( 'gform_field_content', 'gform_form_input_autocomplete', 11, 5 );
function gform_form_input_autocomplete( $input, $field, $value, $lead_id, $form_id ) {
    if ( is_admin() ) return $input;
    if ( GFFormsModel::is_html5_enabled() ) {
        $input = preg_replace( '/<(input|textarea)/', '<${1} autocomplete="fdfsd" ', $input );
    }
    return $input;
}

//////////////////////////////////////////////////////////////////
if (!function_exists('write_log')) {
	function write_log ( $log )  {
		if ( true === WP_DEBUG ) {
			if ( is_array( $log ) || is_object( $log ) ) {
				error_log( print_r( $log, true ) );
			} else {
				error_log( $log );
			}
		}
	}
}
//////////////////////////////////////////////////////////////////
/**
 * Checks if a particular user has a role.
 * Returns true if a match was found.
 * http://docs.appthemes.com/tutorials/wordpress-check-user-role-function/
 *
 * @param string $role Role name.
 * @param int $user_id (Optional) The ID of a user. Defaults to the current user.
 * @return bool
 */
//////////////////////////////////////////////////////////////////
function check_user_role( $role, $user_id = null ) {

	if ( is_numeric( $user_id ) ) {
		$user = get_userdata( $user_id );
	}
	else {
		$user = wp_get_current_user();
	}

	if ( empty( $user ) ) {
		return false;
	}

	return in_array( $role, (array) $user->roles );
}


//////////////////////////////////////////////////////////////////
// Get the slug of the page
//////////////////////////////////////////////////////////////////
function get_the_slug() {
    global $post;
    if ( is_single() || is_page() ) {
        return $post->post_name;
    } else {
        return "";
    }
}
//////////////////////////////////////////////////////////////////
// Custom Excerpt length
//////////////////////////////////////////////////////////////////
function custom_excerpt_length( $length ) {
    return 10;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

//////////////////////////////////////////////////////////////////
// Get the text of a hyperlink
//////////////////////////////////////////////////////////////////
function get_link_text($url) {
    preg_match('/\>(.*)<\/a>/', $url, $matches);
    return $matches[1];
}


//////////////////////////////////////////////////////////////////
// New excerpt more
//////////////////////////////////////////////////////////////////
function custom_excerpt_more( $more ) {
    return '...';//'&hellip;';
}
add_filter('excerpt_more', 'custom_excerpt_more');


//////////////////////////////////////////////////////////////////
// Encode email address to prevent spam bots collecting
// http://davidwalsh.name/php-email-encode-prevent-spam
//////////////////////////////////////////////////////////////////
function encode_email($e) {
    $output = '';
    for ($i = 0; $i < strlen($e); $i++) { $output .= '&#'.ord($e[$i]).';'; }
    return $output;
}

//////////////////////////////////////////////////////////////////
// Validate page/post url
//////////////////////////////////////////////////////////////////
function validate_post_url() {
    global $post;
    $page_url = esc_url(get_permalink( $post->ID ));
    $urlget = strpos($page_url, "?");
    if ($urlget === false) {
        $concate = "?";
    } else {
        $concate = "&";
    }
    return $page_url.$concate;
}

//////////////////////////////////////////////////////////////////
// Check if a url is valid
//////////////////////////////////////////////////////////////////
function is_valid_url($url) {
	return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
}

//////////////////////////////////////////////////////////////////
// is_home() doesn't work outside the loop, so we'll check the slug
//////////////////////////////////////////////////////////////////
function is_homepage() {
    return is_page('home');
}
//////////////////////////////////////////////////////////////////
// Truncate text to nearest word
//////////////////////////////////////////////////////////////////
function truncate_to($string, $your_desired_width) {
	$parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
	$parts_count = count($parts);

	$length = 0;
	$last_part = 0;
	for (; $last_part < $parts_count; ++$last_part) {
		$length += strlen($parts[$last_part]);
		if ($length > $your_desired_width) { break; }
	}

	return implode(array_slice($parts, 0, $last_part)).'&hellip;';
}
//////////////////////////////////////////////////////////////////
// Tiny URL
//////////////////////////////////////////////////////////////////
function getTinyUrl($url) {
	$tinyurl = file_get_contents("http://tinyurl.com/api-create.php?url=".$url);
	return $tinyurl;
}
//////////////////////////////////////////////////////////////////
// Tags for pages
//////////////////////////////////////////////////////////////////
function my_taxonomies() {
    register_taxonomy_for_object_type('post_tag', 'page');
}
add_action('init', 'my_taxonomies');

//////////////////////////////////////////////////////////////////
// Basic Social sharing - TO UPDATE!!!
//////////////////////////////////////////////////////////////////
function social_share_link($social) {
	global $post;
	$post_url = get_the_permalink($post->ID);
	$post_url_encoded = urlencode($post_url);

	if($social == "facebook") {
		echo "https://www.facebook.com/sharer/sharer.php?u=".$post_url_encoded;
	}
    if($social == "google") {
		echo "https://plus.google.com/share?url=".$post_url;
	}
}
//////////////////////////////////////////////////////////////////
// Header Widget
//////////////////////////////////////////////////////////////////
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Currency',
		'id'            => 'head_widget',
		'before_widget' => '<div class="money">',
		'after_widget'  => '</div>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );

