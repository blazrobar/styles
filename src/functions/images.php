<?php

//////////////////////////////////////////////////////////////////
// Add svg MIME Types to WP Media Uploader
//////////////////////////////////////////////////////////////////
function extra_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	$mimes['svgz'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'extra_mime_types' );


//////////////////////////////////////////////////////////////////
// Remove Width and Height Attributes From Inserted Images
//////////////////////////////////////////////////////////////////

function remove_imgsize_attributes( $html ) {
    $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
    return $html;
}
add_filter( 'post_thumbnail_html', 'remove_imgsize_attributes', 10 );
add_filter( 'image_send_to_editor', 'remove_imgsize_attributes', 10 );

//////////////////////////////////////////////////////////////////
// Extend post thumbnail functionality
//////////////////////////////////////////////////////////////////

function custom_image_sizes() {
    add_theme_support('post-thumbnails');

    add_image_size('home-slider', 1600, 500, true);
    add_image_size('full-wdith-image', 1100, 800, false);
    add_image_size('content-image', 800, 800, true);
    add_image_size('content-image-small', 600, 600, true);
    add_image_size('gallery-thumb', 500, 500, true);
    add_image_size('gallery-large', 1200, 3000, false);
    add_image_size('background', 1600, 1200, true);



}
add_action('after_setup_theme', 'custom_image_sizes');

function custom_image_size_names( $imageSizes ) {
    $my_sizes = array(
	    'project-thumb' => 'Project Thumbnail',
	    'product-image' => 'Product Image',
        'large-promo' => 'Large Promo',
        'banner-image' => 'Banner Image'
    );
    return array_merge( $imageSizes, $my_sizes );
}
add_filter( 'image_size_names_choose', 'custom_image_size_names' );

//////////////////////////////////////////////////////////////////
/**
 * Remove standard image sizes so that these sizes are not
 * created during the Media Upload process
 *
 * Tested with WP 3.2.1
 *
 * Hooked to intermediate_image_sizes_advanced filter
 * See wp_generate_attachment_metadata( $attachment_id, $file ) in wp-admin/includes/image.php
 *
 * @param $sizes, array of default and added image sizes
 * @return $sizes, modified array of image sizes
 * @author Ade Walker http://www.studiograsshopper.ch
 */
//////////////////////////////////////////////////////////////////
function filter_image_sizes( $sizes) {

    //unset( $sizes['thumbnail']);
    unset( $sizes['medium']);
    unset( $sizes['large']);

    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'filter_image_sizes');

//////////////////////////////////////////////////////////////////
// Update the crop option on default image sizes
// 1 == force crop
// 2 == proportional
//////////////////////////////////////////////////////////////////
/*
if(false === get_option('medium_crop')) {
    add_option('medium_crop', '1');
}
else {
    update_option('medium_crop', '1');
}
*/

/*
if(false === get_option('large_crop')) {
    add_option('large_crop', '1');
}
else {
    update_option('large_crop', '1');
}
*/
