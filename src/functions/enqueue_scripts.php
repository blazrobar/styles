<?php
function enqueue_custom_scripts() {
	if (!is_admin()) {


        wp_register_script('modernizr', get_bloginfo('template_url') . '/js/vendor/modernizr.custom.js', array(), '2.7.1', true);
        wp_enqueue_script('modernizr');

        wp_register_script('main', get_bloginfo('template_url') . '/js/main.js', array('jquery'), '1.1.3.'.time(), true);
        wp_enqueue_script('main');

		wp_register_style('style', get_template_directory_uri() . '/css/style.css', array( ), '1.2'.time(), 'screen');
		wp_enqueue_style( 'style');
		
	}
}
add_action('init', 'enqueue_custom_scripts');
