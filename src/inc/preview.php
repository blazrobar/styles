<div class="preview-container">
    <div class="gf-demos-container ">
        <?php gravity_form(5, false, false); ?>
        <svg class="browser-top" xmlns="http://www.w3.org/2000/svg"><ellipse fill="#cbd6e2" cx="21" cy="15.996" rx="4" ry="3.996"></ellipse><ellipse fill="#cbd6e2" cx="34" cy="15.996" rx="4" ry="3.996"></ellipse><ellipse fill="#cbd6e2" cx="47" cy="15.996" rx="4" ry="3.996"></ellipse></svg>
        <p class="powered-by">
            <img src="<?php bloginfo('template_directory'); ?>/images/logo.svg" class="logo"/>
            Powered by JetSloth
        </p>
    </div>
</div>