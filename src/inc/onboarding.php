<div class="onboarding-overlay active">
<!--    <div class="onboarding-close">-->
<!--        <img src="--><?php //bloginfo('template_directory'); ?><!--/images/icons/multiply.svg"/>-->
<!--    </div>-->
    <div class="onboaridng-modal">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="onboarding-media" style="background:#ef9340;">
                        <img src="<?php bloginfo('template_directory'); ?>/images/icons/diamond.svg"/>
                    </div>
                    <div class="onboarding-content">
                        <h2>Remix a pre-made form style</h2>
                        <p>Lorem ipsum dolor sit ametitor pellentesque, leo eros rhoncus neque, nec ornare arcu turpis nec velit. Curabitnsectetur adipiscing elit.</p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="onboarding-media" style="background:#d18cf0;">
                        <img src="<?php bloginfo('template_directory'); ?>/images/icons/gesture-device-setting-curve-shape.svg"/>
                    </div>
                    <div class="onboarding-content">
                        <h2>Customise your styles</h2>
                        <p>Lorem ipsum dolor sit ametitor pellentesque, leo eros rhoncus neque, nec ornare arcu turpis nec velit. Curabitnsectetur adipiscing elit.</p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="onboarding-media" style="background:#1abc9c;">
                        <img src="<?php bloginfo('template_directory'); ?>/images/icons/heart.svg"/>
                    </div>
                    <div class="onboarding-content">
                        <h2>Save & share form designs</h2>
                        <p>Lorem ipsum dolor sit ametitor pellentesque, leo eros rhoncus neque, nec ornare arcu turpis nec velit. Curabitnsectetur adipiscing elit.</p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="onboarding-media" style="background:#ff7d5a;">
                        <img src="<?php bloginfo('template_directory'); ?>/images/icons/gesture-device-setting-curve-shape.svg"/>
                    </div>
                    <div class="onboarding-content">
                        <h2>Ready to be creative?</h2>
<!--                        <p>Lorem ipsum dolor sit ametitor pellentesque, leo eros rhoncus neque.</p>-->
                        <a href="#" id="close" class="btn-secondary">Get Started</a>
                    </div>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>