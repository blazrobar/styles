<!--Download Pluign Message-->
<div class="message-model-1">
    <a href="#" id="plugin-download-close" class="close">
        <i class="material-icons">close</i>
    </a>
    <h2>Not a developer?</h2>
    <p>Download and install Gravity Forms custom CSS to easily style your forms using the above CSS code snippets.</p>
    <a href="#" class="btn-secondary">
        <i class="material-icons">assignment_returned</i>Free Download
    </a>
</div>
<!--Requiered Pluign Message 1-->
<div class="message-model-two">
    <i class="material-icons ribbon">bookmark</i>
    <a href="#" id="plugin-download-close-2" class="close">
        <i class="material-icons">close</i>
    </a>
    <p>This style library example uses Collapsible Sections Gravity Forms add-on Download the plugin.
        <a href="#">
            Download the plugin
        </a>
    </p>

</div>
<div class="message-model-3">
    <i class="material-icons ribbon">bookmark</i>
    <a href="#" id="plugin-download-close-3" class="close">
        <i class="material-icons">close</i>
    </a>
    <p>This style library example uses Image Choices Gravity Forms add-on Download the plugin.
        <a href="#">
            Download the plugin
        </a>
    </p>

</div>