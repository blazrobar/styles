<div class="slide-menu-container">
    <div class="naccs">
        <ul class="nacc">
            <li class="active">
                <div class="slider-menu-content-container">
                    <div id="customise_styles"></div>
                    <!--                    <h1>Style Editor</h1>-->
                    <p>Like this style but want to make some changes? Easy, just remix this design and update the design how you wish.</p>
                </div>
                <!--                <div class="slider-menu-css-container">-->
                <!--                    <h2>Advanced Settings</h2>-->
                <!--                    <ul class="display-inputs">-->
                <!--                        <li>-->
                <!--                            <input class="number" type="text" value="5">-->
                <!--                            <label>Border Radius</label>-->
                <!--                        </li>-->
                <!--                        <li>-->
                <!--                            <input class="number" type="text" value="2">-->
                <!--                            <input class="number" type="text" value="3">-->
                <!--                            <input class="number" type="text" value="4">-->
                <!--                            <label>Box Shadow</label>-->
                <!--                        </li>-->
                <!--                        <li>-->
                <!--                            <input class="number" type="text" value="15">-->
                <!--                            <label>Label Font size(px)</label>-->
                <!--                        </li>-->
                <!--                        <li>-->
                <!--                            <input class="number" type="text" value="16">-->
                <!--                            <label>Input Font size</label>-->
                <!--                        </li>-->
                <!--                        <li>-->
                <!--                            <input class="number" type="text" value="2">-->
                <!--                            <label>Input Border size(px)</label>-->
                <!--                        </li>-->
                <!--                    </ul>-->
                <!--                </div>-->
            </li>
            <li>
                <!--CSS-->
                <div class="download-actions">
                    <a href="#" class="btn-primary">
                        <i class="material-icons">assignment_returned</i>Copy CSS
                    </a>
                </div>
                <!--CSS Wrapper-->
                <div class="slider-menu-css-container">
					<span class="css-tag">
						CSS
					</span>

                    <a href="<?php echo site_url(); ?>/raw-css?style_post__id=<?php echo $post->ID; ?>" target="_blank" class="btn-text">
                        View Raw <i class="material-icons">exit_to_app</i>
                    </a>
                    <div class="css-wrapper">
                        <pre class="language-scss"><code><?php echo $css; ?></code></pre>
                    </div>
                </div>
            </li>
            <li>
                <!--Intro Content-->
                <div class="slider-menu-content-container">
                    <h1 contentEditable="true" id="title"><?php the_title();?> (Remix)</h1>
                    <?php the_content();?>
                    <div class="profile-container">
                        <div class="profile-pic">
                            <img src="<?php bloginfo('template_directory'); ?>/images/blaz.jpg"/>
                        </div>
                        <div class="post-data">
                            <a href="#">
                                <i class="material-icons heart">favorite</i>583&emsp;
                            </a>
                            <i class="material-icons">bookmark</i></i>40,289&emsp;
                            <p>By Blaz Robar</p>
                        </div>
                    </div>
                </div>
                <div class="slider-menu-css-container">
                    <h3>Save your changes</h3>
                    <p>Like this style but want to make some changes? Easy, just remix this design and update the design how you wish.</p>
                    <input class="btn-secondary save-button" type="submit" name="save" value="Save style" />
                    <?php $style_id = $_GET['style_post__id'];  ?>
                    <a style="text-align: center; margin-top: 20px; display: block; font-size: 14px;" href="<?php echo get_permalink($style_id)?>"><?php esc_html_e( 'Preview Style', 'textdomain' ); ?></a>
                </div>
            </li>
            <li>
                <div class="slider-menu-content-container">
                    <h1>Settings</h1>
                    <p>Gravity Forms CSS settings</p>
                </div>
            </li>
            <li>
                <div class="slider-menu-content-container">
                    <h1>Liked Themes</h1>
                    <p>Your Like themes</p>
                </div>
            </li>
        </ul>
    </div>
    <div class="slider-trigger-menu-container">
            <a href="#" class="triger">
                <i class="material-icons">sort</i>
            </a>
        <div class="menu">
            <div class="active">
                <a href="#">
                    <i class="material-icons">invert_colors</i>
                </a>
            </div>
            <div>
                <a href="#">
                    <i class="material-icons">get_app</i>
                </a>
            </div>
            <div>
                <a href="#">
                    <i class="material-icons">assignment_ind</i>
                </a>
            </div>
            <div>
                <a href="#">
                    <i class="material-icons">explore</i>
                </a>
            </div>
            <div>
                <a href="#">
                    <i class="material-icons">favorite</i>
                </a>
            </div>
        </div>
    </div>
</div>
