<div class="slide-menu-container">
    <div class="naccs">
        <ul class="nacc">
            <li class="active">
                <div class="slider-menu-content-container">
                    <h3>THEME</h3>
                    <h1><?php the_title();?></h1>
                    <?php the_content();?>
                    <div id="feature_styles"></div>
                    <a href="#" class="btn-primary">Copy CSS</a>
                    <a href="<?php echo home_url('/'); ?>advanced?style_post__id=<?php echo $post->ID; ?>" class="btn-secondary">Advanced Editor</a>
                    <h3>STYLE</h3>
                        <div class="style">
                            <input value="Field" style="font-size: 12px;">
                        </div>
                        <div class="style" style="float: right;">
                            <input value="Field" style="border: none;border-bottom: 2px solid #cdd6e4;background: none;text-indent: 0px;"></div>
                        <div class="style">
                            <input value="Field" style="font-size: 12px;border-radius: 20px;text-indent: 20px;">
                        </div>
                        <div class="style" style="float: right;">
                            <input value="Field" style="border: none;border-left: 2px solid #cdd6e4;background: none;text-indent: 15px;background: none;">
                        </div>
                </div>
            </li>
            <li>
                <div class="slider-menu-css-container">
					<span class="css-tag">
						CSS
					</span>
                    <a href="<?php echo site_url(); ?>/raw-css?style_post__id=<?php echo $post->ID; ?>" target="_blank" class="btn-text">
                        View Raw <i class="material-icons">exit_to_app</i>
                    </a>
                    <div class="css-wrapper">
                        <pre class="language-scss"><code><?php echo $css; ?></code></pre>
                    </div>
                </div>
            </li>
            <li>
                <div class="slider-menu-content-container" >
                    <h1><?php the_title();?></h1>
                    <div class="profile-container">
                        <div class="profile-pic">
                            <img src="<?php bloginfo('template_directory'); ?>/images/blaz.jpg"/>
                        </div>
                        <div class="post-data">
                            <a href="#">
                                <i class="material-icons heart">favorite</i>583&emsp;
                            </a>
                            <i class="material-icons">bookmark</i></i>40,289&emsp;
                            <p>By Blaz Robar</p>
                        </div>
                    </div>
                    <?php the_content();?>
                    <h3>Tagged in</h3>
                    <span class="css-tag">Fun</span>
                    <span class="css-tag">Bright</span>
                    <span class="css-tag">Modern</span>
                    <span class="css-tag">Classy</span>

                </div>
            </li>
        </ul>
    </div>
    <div class="slider-trigger-menu-container">
            <a href="#" class="triger">
                <i class="material-icons">sort</i>
            </a>
        <div class="menu">
            <div class="active">
                <a href="#">
                    <i class="material-icons">invert_colors</i>
                </a>
            </div>
            <div>
                <a href="#">
                    <i class="material-icons">get_app</i>
                </a>
            </div>
            <div>
                <a href="#">
                    <i class="material-icons">assignment_ind</i>
                </a>
            </div>
        </div>
    </div>
</div>
