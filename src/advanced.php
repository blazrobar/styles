<?php
/* Template Name: Advanced
*/
?>

<?php get_header(); ?>
<?php
    $style_id = $_GET['style_post__id'];
?>
<?php
$args = array(
    'post_type'   => 'styles',
    'p' => $style_id
);
    $scheduled = new WP_Query( $args );
    if ( $scheduled->have_posts() ) :
?>
    <?php while( $scheduled->have_posts() ) : $scheduled->the_post() ?>
    <?php
        $theme = [];
        $css_template = get_field('css');
        $css = get_field('css');
        if ( have_rows('variables') ) {
            while ( have_rows('variables') ) {
                the_row();
                $key = get_sub_field( 'variable_name' );
                $colour = get_sub_field( 'variable_colour' );
                $name = get_sub_field( 'display_name' );
	            $simple_key = substr($key, 1);
	            $theme[$simple_key] = [
                    'colour' => $colour,
                    'name' => $name
                ];
	            $css_template = str_replace( $key, '{{'.$simple_key.'}}', $css_template );
	            $css = str_replace( $key, $colour, $css );
            }
        }
    ?>
    <style type="text/css" id="custom_theme_css">
        <?php echo $css; ?>
    </style>
    <script type="text/html" id="custom_theme_css_template">
        <?php echo $css_template; ?>
    </script>
    <script>
        var theme = <?php echo json_encode($theme); ?>;
    </script>


    <style type="text/css">
        .preview-container {
            background:url("http://styles:8888/wp-content/uploads/2019/09/dot-grid.jpg")!important;
        }
    </style>


	<div class="outer-container">
        <?php include(locate_template('inc/preview.php')); ?>
        <?php include(locate_template('inc/menu-advanced.php')); ?>
	</div>
<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
<?php include(locate_template('/inc/onboarding.php')); ?>
<?php get_footer(); ?>