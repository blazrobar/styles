<?php get_header(); ?>
    <section class="style-summary-wrap">
        <?php
        $posts_query = new WP_Query(array(
            'post_type' => 'styles',
            'posts_per_page' => -1,
            'orderby'        => 'name',
            'order'          => 'ASC'
        ));
        ?>
        <?php if ($posts_query->have_posts()): ?>
                <?php while ($posts_query->have_posts()): $posts_query->the_post(); global $post; ?>
                        <?php
                            $theme = [];
                            $css_template = get_field('css_basic');
                            $css = get_field('css_basic');
                                if ( have_rows('variables_basic') ) {
                                    while ( have_rows('variables_basic') ) {
                                        the_row();
                                        $key = get_sub_field( 'variable_name' );
                                        $colour = get_sub_field( 'variable_colour' );
                                        $name = get_sub_field( 'display_name' );
                                        $simple_key = substr($key, 1);
                                        $theme[$simple_key] = [
                                            'colour' => $colour,
                                            'name' => $name
                                        ];

                                        $css_template = str_replace( $key, '{{'.$simple_key.'}}', $css_template );
                                        $css = str_replace( $key, $colour, $css );
                                    };
                                }
                        ?>
                    <div class="style-tile">
                        <script>
                            var theme = <?php echo json_encode($theme); ?>;
                        </script>
                        <a href="<?php the_permalink();?>">
                            <h2><?php the_title();?></h2>
                            <h3>Tagged in</h3>

                            <span class="css-tag">Fun</span>
                            <span class="css-tag">Bright</span>
                            <span class="css-tag">Modern</span>
                            <span class="css-tag">Classy</span>
                            <div class="index_styles">
                                <button type="button" class="pcr-button" style="background: <?php echo $colour ;?>"></button>
                            </div>
                            <?php the_content();?>
                        </a>
                    </div>
                <?php endwhile; ?>
            <?php endif; wp_reset_postdata(); ?>
    </section>

<?php //include(locate_template('/inc/onboarding.php')); ?>
<?php get_footer(); ?>