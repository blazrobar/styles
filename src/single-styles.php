<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php
    $theme = [];
    $css_template = get_field('css_basic');
    $css = get_field('css_basic');
    if ( have_rows('variables_basic') ) {
        while ( have_rows('variables_basic') ) {
            the_row();
            $key = get_sub_field( 'variable_name' );
            $colour = get_sub_field( 'variable_colour' );
            $name = get_sub_field( 'display_name' );
            $simple_key = substr($key, 1);
            $theme[$simple_key] = [
                'colour' => $colour,
                'name' => $name
            ];
            $css_template = str_replace( $key, '{{'.$simple_key.'}}', $css_template );
            $css = str_replace( $key, $colour, $css );
        }
    }
    ?>
    <style type="text/css" id="custom_theme_css">
        <?php echo $css; ?>
    </style>
    <script type="text/html" id="custom_theme_css_template">
        <?php echo $css_template; ?>
    </script>
    <script>
        var theme = <?php echo json_encode($theme); ?>;
    </script>



	<div class="outer-container">
        <?php include(locate_template('inc/preview.php')); ?>
        <?php include(locate_template('inc/menu-basic.php')); ?>
	</div>
<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
<?php //include(locate_template('/inc/onboarding.php')); ?>
<?php //include(locate_template('inc/plugin-prompts.php')); ?>
<?php get_footer(); ?>