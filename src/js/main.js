(function($){

	'use strict';

	var $doc = $(document);

	$doc.ready(function(){
		var $window = $(window);
		var $body = $('body');
		var $htmlBody = $('html, body');

		var windowWidth = $window.width();
		var windowHeight = $window.height();

		// Set the correct values for the breakpoints to match Bootstrap etc
		var smallBreakpoint = 420;
		var mediumBreakpoint = 769;
		var largeBreakpoint = 1024;
		var isSmall;
		var isMedium;
		var isLarge;
		var isExtraLarge;
		var resizeTasks = [];

		/***************************
		 *
		 *	Tab Nav
		 *
		 ***************************/
		var tabMenu = $('.slider-trigger-menu-container .menu > div');
		var tabContent = $('.slide-menu-container ul.nacc > li');

		tabMenu.on('click',  function() {
			var i = $(this).index();
			tabMenu.removeClass('active');
			$(this).addClass('active');
			tabContent.removeClass('active');
			tabContent.filter(':eq(' + i + ')').addClass('active');
		});



		/***************************
		 *
		 *	NAVS
		 *
		 ***************************/

		$('.nav-mobile').on('click', function(e){
			$(this).toggleClass('active');

			$('.wrapper').toggleClass('expanded');

			e.preventDefault();
		});


		/***************************
		 *
		 *	Close message 1
		 *
		 ***************************/
		$('.message-model-1').delay( 1000 ).fadeIn( "slow" );
		$('.message-model-two').delay( 2200 ).fadeIn( "slow" );
		// $('.message-model-3').delay( 2400 ).fadeIn( "slow" );

		$('#plugin-download-close').on('click', function(){
			$('.message-model-1').fadeOut();
		});
		$('#plugin-download-close-2').on('click', function(){
			$('.message-model-two').fadeOut();
		});
		$('#plugin-download-close-3').on('click', function(){
			$('.message-model-3').fadeOut();
		});
		$('.triger').on('click', function(){
			$('.slide-menu-container').toggleClass('menuClose');
			$('.preview-container').toggleClass('wide');
		});
		/***************************
		 *
		 *	RESPONSIVE / BREAKPOINTS
		 *
		 ***************************/

		function onWindowResize() {
			// Check breakpoints
			windowWidth = $window.width();
			windowHeight = $window.height();
			isSmall = (windowWidth <= smallBreakpoint);
			isMedium = (windowWidth > smallBreakpoint && windowWidth <= mediumBreakpoint);
			isLarge = (windowWidth > mediumBreakpoint);
			isExtraLarge = (windowWidth > largeBreakpoint);

			$.each(resizeTasks, function(i, func){
				if (typeof func === 'function') {
					func();
				}
			});
		}
		/***************************
		 *
		 *  On-boarding Swipper & Lightbox controls
		 *
		 ***************************/
		var onboardingSwipper = new Swiper ('.swiper-container', {
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				// type: 'progressbar',
			}
		});
		var closeButton = $('.onboarding-close');
		var overlay = $('.onboarding-overlay');
		var close = $('#close');

		closeButton.on('click',  function() {
			overlay.removeClass('active');
			overlay.addClass('disable');
		});
		close.on('click',  function() {
			overlay.removeClass('active');
			overlay.addClass('disable');
		});





		/***************************
		 *
		 *  Save Button
		 *
		 ***************************/
		$('.save-button').click(function(){
			// alert("Button clicked");
			var clickBtnValue = $(this).val();
			var ajaxurl = 'http://styles:8888/wp-content/themes/Styles/ajax.php',
				data =  {'action': clickBtnValue};
			$.post(ajaxurl, data, function (response) {
				// clickBtnValue.value="saved";
				alert("action performed successfully");
			});
		});





		/***************************
		 *
		 *  Color Pickers
		 *
		 ***************************/

		var $themeCSS = $('#custom_theme_css');
		var $cssDisplay = $('.css-wrapper:first code');
		var $cssTemplate = $('#custom_theme_css_template').html();
		window.pickers = {};

		function updateCss() {
			var newCss = $cssTemplate.repeat(1);
			$.each(window.theme, function(key, data){
				newCss = newCss.replace( new RegExp('{{'+key+'}}', 'g'), data.colour );
			});
			$themeCSS.empty().text(newCss);
			$cssDisplay.empty().text(newCss);
			Prism.highlightAll();
		}


		function onPickerColourChange(color, instance){
			var key = $(instance.options.el).data('key');
			window.theme[key].colour = color.toHEXA().toString();
			updateCss();
		}


		function initPickers() {
			var swatches = [];
			$.each(window.theme, function(key, data){ swatches.push(window.theme[key].colour.toString()); });
			$.each(window.theme, function(key, data){
				var ID = 'picker_' + key;
				var hex_swatch = window.theme[key].colour.toString();
				console.log(hex_swatch);
				var themeItemMarkup = [
					'<div id="' + ID + '" class="theme-item">',
						'<span class="theme-item__picker" data-key="' + key + '"></span>',
						'<span class="theme-item__name">' + data.name + '</span>',
					'</div>'
				].join('');
				$('#customise_styles').prepend(themeItemMarkup);
				$('#feature_styles').prepend(themeItemMarkup);

				var picker = Pickr.create({ el: '#'+ID+' .theme-item__picker',
					theme: 'nano', // or 'monolith', or 'nano'
					default: data.colour,
					swatches: swatches,
					comparison: false,
					components: {
						// Main components
						preview: true,
						opacity: true,
						hue: true,
						// Input / output Options
						interaction: {
							hex: true,
							rgba: true,
							hsla: false,
							hsva: false,
							cmyk: false,
							input: true,
							clear: false,
							save: true
						}
					}
				});

				picker.on('change', onPickerColourChange);
				window.pickers[key] = picker;
			});

		}

		if (typeof window.theme !== 'undefined') {
			initPickers();
		}

	});


})(jQuery);